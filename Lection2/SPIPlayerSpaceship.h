//
//  PlayerSpaceship.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"
#import "SPIStarSystem.h"
#import "SPIGalaxy.h"
#import "SPIGameObject.h"

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipCommand) {
    SPIPlayerSpaceshipCommandFlyToPreviousObject = 1,
    SPIPlayerSpaceshipCommandExploreCurrentObject = 2,
    SPIPlayerSpaceshipCommandFlyToNextObject = 3,
    SPIPlayerSpaceshipCommandFlyToAnotherSystem = 4,
    SPIPlayerSpaceshipCommandDestroyCurrentObject = 5,
    SPIPlayerSpaceshipCommandExit = 42,
};

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipResponse) {
    SPIPlayerSpaceshipResponseOK = 0,
    SPIPlayerSpaceshipResponseExit,
};

@interface SPIPlayerSpaceship : NSObject <SPIGameObject>

@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, assign) SPIPlayerSpaceshipCommand nextCommand;
@property (nonatomic, weak, setter=loadGalaxy:) SPIGalaxy *galaxy;
@property (nonatomic, weak, setter=loadStarSystem:) SPIStarSystem *starSystem;
@property (nonatomic, weak) SPISpaceObject *currentSpaceObject;
@property (nonatomic, assign) int nextSystem;
@property (nonatomic, assign) int destroyType;

- (instancetype)initWithName:(NSString *)name;
- (BOOL) getYesOrNo;
- (NSString *)availableCommands;
- (NSString *)availableSystems;

- (SPIPlayerSpaceshipResponse)waitForCommand;
- (int)chooseAnotherSystem;

@end
