//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"
#import "SPIStar.h"
#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"
#import "SPIGalaxy.h"


int main(int argc, const char * argv[]) {    
    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
    
    SPIStarSystem *betaStarSystem = [[SPIStarSystem alloc] initWithName:@"Orion" age:@(500000000)];
    
    SPIStarSystem *gammaStarSystem = [[SPIStarSystem alloc] initWithName:@"Europa" age:@(700000000)];
    
    SPIStarSystem *deltaStarSystem = [[SPIStarSystem alloc] initWithName:@"Guise" age:@(100000)];
    
    SPIStar *alphaStar = [[SPIStar alloc] initWithName:@"AlphaStar"];
    alphaStar.typeOfStar = 2;
    alphaStar.starWeigth = [NSNumber numberWithLong:6000000000];
    
    SPIStar *betaStar = [[SPIStar alloc] initWithName:@"BetaStar"];
    betaStar.typeOfStar = 1;
    betaStar.starWeigth = [NSNumber numberWithLong:7000000000];
    
    SPIStar *gammaStar = [[SPIStar alloc] initWithName:@"GammaStar"];
    gammaStar.typeOfStar = 1;
    gammaStar.starWeigth = [NSNumber numberWithLong:8000000000];
    
    SPIStar *deltaStar = [[SPIStar alloc] initWithName:@"DeltaStar"];
    gammaStar.typeOfStar = 0;
    gammaStar.starWeigth = [NSNumber numberWithLong:500000];
    
    SPIPlanet *alpha1 = [[SPIPlanet alloc] initWithName:@"Alpha 1"];
    alpha1.atmosphere = YES;
    alpha1.invulnerable = YES;
    alpha1.peoplesCount = 325000000;

    SPIPlanet *alpha2 = [[SPIPlanet alloc] initWithName:@"Alpha 2"];
    alpha2.atmosphere = YES;
    alpha2.peoplesCount = 700000000;
    
    SPIPlanet *alpha3 = [[SPIPlanet alloc] initWithName:@"Alpha 3"];
    alpha3.atmosphere = YES;
    alpha3.peoplesCount = 625000000;
    
    SPIPlanet *alpha4 = [[SPIPlanet alloc] initWithName:@"Alpha 4"];
    alpha4.atmosphere = NO;
    
    SPIPlanet *beta1 = [[SPIPlanet alloc] initWithName:@"Beta 1"];
    beta1.atmosphere = YES;
    beta1.invulnerable = YES;
    beta1.peoplesCount = 223000000;
    
    SPIPlanet *beta2 = [[SPIPlanet alloc] initWithName:@"Beta 2"];
    beta2.atmosphere = NO;
    
    SPIPlanet *beta3 = [[SPIPlanet alloc] initWithName:@"Beta 3"];
    beta3.atmosphere = YES;
    beta3.peoplesCount = 10000;
    
    SPIPlanet *beta4 = [[SPIPlanet alloc] initWithName:@"Beta 4"];
    beta4.atmosphere = NO;
    
    SPIPlanet *gamma1 = [[SPIPlanet alloc] initWithName:@"Gamma 1"];
    beta1.atmosphere = YES;
    beta1.peoplesCount = 223000000;
    
    SPIPlanet *gamma2 = [[SPIPlanet alloc] initWithName:@"Gamma 2"];
    beta2.atmosphere = NO;
    
    SPIPlanet *gamma3 = [[SPIPlanet alloc] initWithName:@"Gamma 3"];
    beta3.atmosphere = YES;
    beta3.peoplesCount = 10000;
    
    SPIPlanet *delta1 = [[SPIPlanet alloc] initWithName:@"Delta 1"];
    beta1.atmosphere = NO;
    
    SPIPlanet *delta2 = [[SPIPlanet alloc] initWithName:@"Delta 2"];
    beta2.atmosphere = NO;
    
    SPIAsteroidField *alphaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"AlphaField"];
    alphaAsteroidField.density = 6000000;
    
    SPIAsteroidField *betaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"BetaField"];
    betaAsteroidField.density = 130000;
    
    SPIAsteroidField *gammaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"GammaField"];
    betaAsteroidField.density = 1305000;
    
    alphaStarSystem.spaceObjects = [@[alphaStar, alpha1, alphaAsteroidField, alpha2, alpha3, alpha4] mutableCopy];
    
    betaStarSystem.spaceObjects = [@[betaStar, beta1, beta2, betaAsteroidField, beta3, beta4]mutableCopy];
    
    gammaStarSystem.spaceObjects = [@[gammaStar, gamma1, gamma2, gammaAsteroidField, gamma3]mutableCopy];
    
    deltaStarSystem.spaceObjects = [@[deltaStar, delta1, delta2]mutableCopy];
    
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Falcon"];
    
    SPIGalaxy *milkyWay = [[SPIGalaxy alloc] initWithName:@"MilkyWay"];
    
    milkyWay.spaceSystems = [@[alphaStarSystem, betaStarSystem, gammaStarSystem, deltaStarSystem]mutableCopy];
    
    [spaceship loadGalaxy:milkyWay];
    spaceship.nextSystem = 99;
    spaceship.destroyType = 99;
    int iterator = 1;
    
    BOOL play = YES;
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];

    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
        
        switch (spaceship.destroyType) {
            case 1:{
                NSInteger currentSpaceObjectIndex = [spaceship.starSystem.spaceObjects indexOfObject:spaceship.currentSpaceObject];
                SPIPlanet *planet = [[SPIPlanet alloc] initWithName:[NSString stringWithFormat:@"Generated Planet %d",iterator]];
                planet.atmosphere = spaceship.getYesOrNo;
                planet.peoplesCount = arc4random_uniform(99999999);
                spaceship.currentSpaceObject = planet;
                [spaceship.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:planet];
                spaceship.currentSpaceObject = [spaceship.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex];
                iterator = iterator + 1;
                spaceship.destroyType = 99;
                break;
            }
            case 2:{
                NSInteger currentSpaceObjectIndex = [spaceship.starSystem.spaceObjects indexOfObject:spaceship.currentSpaceObject];
                SPIAsteroidField *asteroidField = [[SPIAsteroidField alloc] initWithName:[NSString stringWithFormat:@"Generated Asteroid Field %d",iterator]];
                asteroidField.density = arc4random_uniform(999999);
                spaceship.currentSpaceObject = asteroidField;
                [spaceship.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:asteroidField];
                spaceship.currentSpaceObject = [spaceship.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex];
                iterator = iterator + 1;
                spaceship.destroyType = 99;
                break;
            }
            case 3:{
                NSInteger currentSpaceObjectIndex = [spaceship.starSystem.spaceObjects indexOfObject:spaceship.currentSpaceObject];
                NSInteger currentSpaceObjectsCount = spaceship.starSystem.spaceObjects.count;
                NSInteger currentSpaceSystemIndex = [spaceship.galaxy.spaceSystems indexOfObject:spaceship.starSystem];
                NSInteger currentSpaceSystemCount = spaceship.galaxy.spaceSystems.count;
                BOOL systemDestroyed = NO;
                //printf("\n Objects in system before delete == %lu", (unsigned long)spaceship.starSystem.spaceObjects.count);
                //printf("\n currentSpaceObjectIndex before delete == %lu", (unsigned long)[spaceship.starSystem.spaceObjects indexOfObject:spaceship.currentSpaceObject]);
                //printf("\n Space systems before delete == %lu", (unsigned long)spaceship.galaxy.spaceSystems.count);
                //printf("\n currentSpaceSystemIndex before delete == %lu", (unsigned long)[spaceship.galaxy.spaceSystems indexOfObject:spaceship.starSystem]);
                
                if (currentSpaceObjectsCount == 1) {
                    if (currentSpaceSystemCount == 1){
                    printf("\n THE END : GALAXY NOT EXIST !!! \n");
                        [spaceship.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
                        play = NO;
                        continue;
                    }
                    if ((currentSpaceSystemIndex+1) == currentSpaceSystemCount){
                        [spaceship.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
                        [spaceship.galaxy.spaceSystems removeObjectAtIndex:currentSpaceSystemIndex];
                        [spaceship loadStarSystem:[milkyWay.spaceSystems objectAtIndex:currentSpaceSystemIndex-1]];
                        systemDestroyed = YES;
                    }
                    if (((currentSpaceSystemIndex+1) >= 1)&&((currentSpaceSystemIndex+1)<currentSpaceSystemCount)){
                        [spaceship.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
                        [spaceship.galaxy.spaceSystems removeObjectAtIndex:currentSpaceSystemIndex];
                        [spaceship loadStarSystem:[milkyWay.spaceSystems objectAtIndex:currentSpaceSystemIndex+1]];
                        systemDestroyed = YES;
                    }
                }
                if ((systemDestroyed == NO)&&((currentSpaceObjectIndex+1) == currentSpaceObjectsCount)){
                    spaceship.currentSpaceObject = [spaceship.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex-1];
                    [spaceship.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
                }
                if ((systemDestroyed == NO)&&(((currentSpaceObjectIndex+1) >= 1)&&((currentSpaceObjectIndex+1)<currentSpaceObjectsCount))){
                    spaceship.currentSpaceObject = [spaceship.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex+1];
                    [spaceship.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
                }
                
                //printf("\n Objects in system after delete == %lu", (unsigned long)spaceship.starSystem.spaceObjects.count);
                //printf("\n currentSpaceObjectIndex after delete == %lu", (unsigned long)[spaceship.starSystem.spaceObjects indexOfObject:spaceship.currentSpaceObject]);
                //printf("\n Space systems after delete == %lu", (unsigned long)spaceship.galaxy.spaceSystems.count);
                //printf("\n currentSpaceSystemIndex after delete == %lu", (unsigned long)[spaceship.galaxy.spaceSystems indexOfObject:spaceship.starSystem]);

                spaceship.destroyType = 99;
                break;
            }
                
            default:
                break;
        }
        
        if (spaceship.destroyType){
            
        }
        
        if (spaceship.nextSystem != 99) {
            [spaceship loadStarSystem:[milkyWay.spaceSystems objectAtIndex:spaceship.nextSystem]];
            spaceship.nextSystem = 99;
        }
    }
    return 0;
}


