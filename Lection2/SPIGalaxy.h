//
//  SPIGalaxy.h
//  Lection2
//
//  Created by Admin on 06.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPIGalaxy : NSObject

@property (nonatomic, copy, readonly) NSString *name;

@property (nonatomic, strong) NSMutableArray *spaceSystems;

- (instancetype)initWithName:(NSString *)name;

- (NSString *)title;

@end
