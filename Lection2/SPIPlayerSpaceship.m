//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

-(BOOL) getYesOrNo
{
    int tmp = (arc4random() % 30)+1;
    if(tmp % 5 == 0)
        return YES;
    return NO;
}

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
        _nextSystem = 99;
        _destroyType = 99;
    }
    return self;
}

- (void)loadGalaxy:(SPIGalaxy *)galaxy {
    _galaxy = galaxy;
    _starSystem = _galaxy.spaceSystems.firstObject;
    _currentSpaceObject = _starSystem.spaceObjects.firstObject;
}

- (void)loadStarSystem:(SPIStarSystem *)starSystem {
    _starSystem = starSystem;
    _currentSpaceObject = _starSystem.spaceObjects.firstObject;
}

- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];

    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    int commandNumber = atoi(command);
    self.nextCommand = commandNumber;
    
    return self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (int)chooseAnotherSystem {
    char system[255];
    //!!!
    printf("\n%s", [[self availableSystems] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Selected system:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(system, 255, stdin);
    int systemNumber = atoi(system);
    if ((systemNumber == [self.galaxy.spaceSystems indexOfObject:self.starSystem])||(systemNumber>self.galaxy.spaceSystems.count -1)){
        return self.chooseAnotherSystem;
    }
    return systemNumber;
}

- (void)nextTurn {
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToAnotherSystem: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if ((currentSpaceObjectIndex == (self.starSystem.spaceObjects.count - 1))||(currentSpaceObjectIndex == 0)) {
                self.nextSystem = self.chooseAnotherSystem;
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroyCurrentObject:{
            if (!self.currentSpaceObject.invulnerable){
                switch (self.currentSpaceObject.type) {
                    case SPISpaceObjectTypeStar:{
                        printf("\n Star was destroyed!!! \n");
                        self.destroyType = 1;
                        break;
                    }
                    case SPISpaceObjectTypePlanet:{
                        printf("\n Planet was destroyed!!! \n");
                        self.destroyType = 2;
                        break;
                    }
                    case SPISpaceObjectTypeAsteroidField:{
                        printf("\n Asteroid Field was destroyed!!! \n");
                        self.destroyType = 3;
                        break;
                    }
                    default:{
                        printf("\n Unknown was destroyed!!! \n");
                        break;
                    }
                }
            }
            break;
        }
        default:
            break;
    }
    self.nextCommand = 0;
}

- (NSString *)availableCommands {
    if (self.starSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandDestroyCurrentObject; command++) {
            NSString *description = [self descriptionForCommand:command];
            if (description) {
                [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
            }
        }
        
        NSString *description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

- (NSString *)availableSystems {
    NSInteger currentSystemIndex = [self.galaxy.spaceSystems indexOfObject:self.starSystem];
    NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
    for (NSInteger system = 0; system <= self.galaxy.spaceSystems.count -1; system++){
        if (system != currentSystemIndex){
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %s Star System", system, [[[self.galaxy.spaceSystems objectAtIndex:system] title] UTF8String]]];
        }
    }
    
    return [mutableDescriptions componentsJoinedByString:@"\n"];
}

- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToAnotherSystem: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if ((currentSpaceObjectIndex == (self.starSystem.spaceObjects.count - 1))||(currentSpaceObjectIndex == 0)) {
                commandDescription = [NSString stringWithFormat:@"Fly to another star system"];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroyCurrentObject:{
            if (!self.currentSpaceObject.invulnerable){
                commandDescription = [NSString stringWithFormat:@"Destroy %@", [self.currentSpaceObject title]];
            }
            break;
        }
            
        case  SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@"Exit game"];
            break;
        }
            
        default:
            break;
    }
    return commandDescription;
}


@end
