//
//  SPISpaceObject+SPIStar.m
//  Lection2
//
//  Created by Admin on 05.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIStar.h"

@implementation SPIStar

- (instancetype)initWithName:(NSString *)name {
    self = [super initWithType:SPISpaceObjectTypeStar name:name];
    if (self) {
        
    }
    return self;
}

- (NSString*)describeStarType {
    switch(self.typeOfStar) {
        case 0:
            return @"Midget";
        case 1:
            return @"Usual";
        case 2:
            return @"Giant";
        case NSIntegerMax:
            return @"Unknown";
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nStar: %@ \nInvulnerable: %@\nStar Type: %@ \nStar Weigth: %@",
            self.name,
            self.invulnerable ? @"YES" : @"NO",
            self.describeStarType,
            self.starWeigth];
}

@end
