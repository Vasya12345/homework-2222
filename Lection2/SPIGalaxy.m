//
//  SPIGalaxy.m
//  Lection2
//
//  Created by Admin on 06.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIGalaxy.h"

@implementation SPIGalaxy

- (instancetype)init {
    return [self initWithName:@"Milky Way"];
}

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nGalaxy: %@", self.name];
}

- (NSString *)title {
    return [NSString stringWithFormat:@"%@", self.name];
}

@end
