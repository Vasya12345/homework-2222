//
//  SPISpaceObject+SPIStar.h
//  Lection2
//
//  Created by Admin on 05.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

typedef NS_ENUM(NSInteger, StarType){
    StarTypeMidget = 0,
    StarTypeUsual = 1,
    StarTypeGiant = 2,
    StarTypeUnknown = NSIntegerMax
};

@interface SPIStar : SPISpaceObject

@property (nonatomic, assign) StarType typeOfStar;
@property (nonatomic, strong) NSNumber *starWeigth;


- (instancetype)initWithName:(NSString *)name;

@end
